#!/usr/bin/env python3
"""This module provides an `Hypervisor` implementation for the LXD container
manager.

This is the current reference implementation of the `Hypervisor` class.

"""
# pylint: disable=too-many-arguments       # limit should be raised
# pylint: disable=no-member                # pylxd not typed properly
from asyncio import get_running_loop
from threading import current_thread
from time import monotonic, sleep
from typing import Dict, ForwardRef, TypeVar

from pylxd.client import Client
from pylxd.models.container import Container, Snapshot

from .config import Config
from .exceptions import DegradedService, ServiceTimeoutError, StoppedService
from .hypervisor import Hypervisor
from .transport import Transport
from .utils import AsyncMixin, async_background

# pylint: disable=invalid-name
ContainerExecuteResult = TypeVar('ContainerExecuteResult')
Service = ForwardRef('Service')
# pylint: enable=invalid-name


class LXDHypervisor(Hypervisor, AsyncMixin, type="lxd"):
    """LXD implementation of the `Hypervisor` class.

    As the Python LXD API client, `pylxd`, does not have async support, we have
    to schedule our methods on a secondary thread, using `@async_background`.

    """

    def __init__(self, config: Config, transport: Transport, **kwargs):
        super().__init__(config, transport, **kwargs)
        self.__client: Client = None
        self.__containers: Dict[Service, Container] = dict()

    # Runtime properties
    # -------------------------------------------------------------------------

    @property
    def _client(self) -> Client:
        """API client object, lazily initialized. """
        loop = get_running_loop()
        if loop is not None and loop.is_running():
            # pylint: disable=protected-access
            # XXX: no better solution
            assert current_thread().ident != loop._thread_id
        if self.__client is None:
            self.__client = Client(endpoint=self.endpoint, version=self.version,
                                   verify=self.verify, timeout=self.timeout)
        return self.__client

    # Internal methods
    # ------------------------------------------------------------------------

    def _check_container(self, service: Service, container: Container):
        """Determine if a container is properly running, raising an error if it
        has reached a degraded state.

        """
        command = ["/bin/sh", "-lc", "systemctl is-system-running"]
        result: ContainerExecuteResult = container.execute(command)
        if result.stdout == "running\n":
            return True
        if result.stdout == "degraded\n":
            self.log.warning("service {service.name} is degraded")
            message = "Service is degraded"
            raise DegradedService(service, message)
        self.log.debug("service {service.name} is not running yet")
        return False

    @staticmethod
    def _is_running(container: Container):
        """Determine if a container is considered as started by the LXD
        hypervisor.

        """
        return container.state().status != "Running"

    def _enable_interface(self, container: Container, interface: str):
        pass  # TODO write _enable_interface()

    def _disable_interface(self, container: Container, interface: str):
        pass  # TODO write _disable_interface

    # Public methods
    # -------------------------------------------------------------------------

    @async_background
    def query(self, service):
        pass  # TODO write query()

    @async_background
    def create(self, service, config, name=None):
        if name is None:
            name = service.name
        self._client.containers.create(dict(config, name=name), wait=False)

    @async_background
    def start(self, service, timeout=None, force=True):
        container = self._client.containers.get(service.name)
        container.start(force=force, timeout=timeout, wait=True)

    @async_background
    def stop(self, service, timeout=None, force=True):
        container = self._client.containers.get(service.name)
        container.stop(force=force, timeout=timeout, wait=True)

    @async_background
    def unmount(self, service: Service, name):
        container = self._client.containers.get(service.name)
        self._disable_interface(container, 'uplink')
        container.rename(name, wait=True)

    @async_background
    def mount(self, service, new, old=None):
        new_container = self._client.containers.get(new)
        if old is not None:
            old_container = self._client.containers.get(service.name)
            if old_container is not None:
                old_container.rename(old, wait=True)
                self._disable_interface(old_container, 'uplink')
        self._enable_interface(new_container, 'uplink')
        new_container.rename(service.name, wait=True)

    @async_background
    def wait(self, service, interval=1000, timeout=None):
        container = self._client.containers.get(service.name)
        started, start = False, monotonic()
        while monotonic() - timeout > start:
            if self._check_container(service, container):
                return
            if not self._is_running(container):
                if started:
                    raise StoppedService(service)
                container.start()
                started = True
            sleep(interval)
        raise ServiceTimeoutError(service, timeout)

    @async_background
    def _reboot(self, service, timeout=None):
        container = self._client.containers.get(service.name)
        container.reboot(timeout=timeout)

    async def reboot(self, service, timeout=None,
                     expect=None, keep_enabled=False):
        await self._reboot(service, timeout=timeout)

    @async_background
    def snapshot(self, service, name) -> Snapshot:
        container = self._client.containers.get(service.name)
        return container.snapshots.create(name, wait=True)

    @async_background
    def restore(self, service, snapshot):
        snapshot.restore(wait=True)

    @async_background
    def cleanup(self, service, snapshot):
        snapshot.delete(wait=True)

    @async_background
    def delete(self, service, timeout=None, force=False):
        container = self._client.containers.get(service.name)
        container.delete(timeout=timeout, force=force)

    @async_background
    def wipe(self, service):
        container = self._client.containers.get(service.name)
        for device in container.expanded_devices:
            container.delete(wait=True)
            if device["type"] != "disk" or device["path"] == "/":
                continue
            storage_pool = self._client.storages.get(device["pool]"])
            volume = storage_pool.volumes.get(device["source"])
            volume.delete(wait=True)
