#!/usr/bin/env python3
"""Configuration management backends are used by the `Host` and `Service`
classes to manage the system configuration of hosts and services. Backends
should provide methods to deploy a system environment to target systems, and run
some management tasks (such as disabling/enabling services).

Current implementations :
 - NixOS

Planned implementations :
 - Custom scripts
 - Ansible

"""
# pylint: disable=too-few-public-methods
from abc import abstractmethod

from .config import Config, Snapshot
from .transport import Transport
from .utils import AsyncMixin, LogMixin, PluginBase


class Backend(PluginBase, AsyncMixin, LogMixin):
    """Base class for host/service configuration backends.

    Current implementations:
     - `nixos` : Deploy NixOS configuration
                 (supports local/remote builds and quick rollbacks)

    Future implementations:
     - `ansible` : Deploy Ansible playbook

    """

    @classmethod
    def from_config(cls, config, *args, **kwargs):
        """Use the provided configuration to find the correct implementation,
        then instantiate this implementation.

        """
        return cls[config["type"]](config, *args, **kwargs)

    def __init__(self, config: Config, transport: Transport, **kwargs):
        super().__init__(**kwargs)
        self._config = config
        self._transport = transport

    @abstractmethod
    async def backup(self) -> Snapshot:
        """TODO"""

    @abstractmethod
    async def restore(self, snapshot: Snapshot):
        """TODO"""

    @abstractmethod
    async def cleanup(self, snapshot: Snapshot):
        """TODO"""

    @abstractmethod
    async def update(self):
        """TODO"""
