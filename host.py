#!/usr/bin/env python
"""This module provides a `Host` class that represents an intermediate `Node` of
the infrastructure, and provides host management methods, that are implemented
using the `Backend` and `Transport` classes.

Related modules :
 - `autonix.transport` :: Management transport, used by configuration management
                        backends to talk to the host.
 - `autonix.backend` :: Configuration management providers, used to deploy the
                        system configuration of hosts and services.

"""
from typing import List

from .backend import Backend
from .domain import Domain
from .hypervisor import Hypervisor
from .node import Config, Node, Snapshot
from .service import Service
from .transport import Transport
from .utils import Log, async_progress


class Host(Node):
    """This class is used to represent managed hosts, store their runtimhce
    state and child classes, and provide management methods implemented using
    `Transport` and `Backend`.

    Hosts manage instances of the `Transport`, `Hypervisor` and `Service`
    classes.

    """

    # Initializer
    # -------------------------------------------------------------------------

    def __init__(self, config: Config, *args, log: Log = None, **kwargs):
        super().__init__(*args, **kwargs)
        # setup parameters
        self._config = config
        self.log = Log(f"host:{self.qualified_name}", parent=log)
        # setup transport and configuration backend
        tp_config, tp_type = config["transport"], config["transport"]["type"]
        bk_config, bk_type = config["backend"], config["backend"]["type"]
        self._transport = Transport[tp_type](tp_config, log=self.log)
        self._backend = Backend[bk_type](bk_config, self.transport, log=self.log)
        # create child client classes
        self._hypervisors = dict(self._init_hypervisors(**kwargs))
        self._domains = dict(self._init_domains(**kwargs))
        self._services = dict(self._init_services(**kwargs))
        # report initialization in debug log
        self.log.debug("initialized client class")
        self.log.debug("setup address: {self.address}")
        self.log.debug("configuration backend: {self._backend}")
        self.log.debug("remote transport: {self._transport}")
        self.log.debug(
            f"got {len(self._hypervisors)} hypervisors, "
            f"{len(self._domains)} domains, "
            f"{len(self._services)} services"
        )

    def _init_hypervisors(self, **kwargs):
        for name, config in self._config["hypervisors"].items():
            if config["enable"]:
                yield name, Hypervisor.from_config(
                    name, config, self.transport, **kwargs
                )

    def _init_domains(self, **kwargs):
        for name, config in self._config["domains"].items():
            yield name, Domain(config, self, **kwargs)

    def _init_services(self, **kwargs):
        for domain in self._domains.values():
            for name, config in domain.config["services"].items():
                hypervisor = self.hypervisors[config["type"]]
                yield name, Service.from_config(
                    config, self, domain, hypervisor, **kwargs
                )

    # Public properties
    # -------------------------------------------------------------------------

    @property
    def name(self) -> str:
        """Name of this host. """
        return self._config["name"]

    @property
    def qualified_name(self) -> str:
        """Fully qualified name of this host. """
        return self._config["qualifiedName"]

    @property
    def address(self) -> str:
        """Address used to reach this host. """
        return self._config["address"]["setup"]

    # Configuration management
    # -------------------------------------------------------------------------

    @async_progress("saving snapshot")
    def snapshot(self):
        """TODO"""
        return self._backend.snapshot()

    @async_progress("rolling back")
    def restore(self, snapshot: Snapshot):
        """TODO"""
        self._backend.rollback(snapshot)


Hosts = List[Host]
