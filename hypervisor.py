#!/usr/bin/env python3
"""Hypervisor providers are used by `Host` and ``Service` classes to manage the
hypervisor running the service containers. Hypervisors should provide container
management methods (such as creating/deleting containers).

Hypervisors typically used their own transport to communicate with remote hosts,
but they may also use the host's `Transport` instance. AutoNix expects the
Hypervisor implementations to provide support for quick snapshots/restores,
which may be faked using cached rebuilds.

Current implementations :
 - LXD

Planned implementations :
 - Firecracker
 - (?) CRI

"""
# pylint: disable=too-few-public-methods
from typing import ForwardRef

from .config import Config, Snapshot
from .transport import Transport
from .utils import LogMixin, PluginBase

# pylint: disable=invalid-name
Service = ForwardRef('Service')
# pylint: enable=invalid-name


class Hypervisor(PluginBase, LogMixin):
    """Base class for service hypervisor."""

    @classmethod
    def from_config(cls, name, *args, **kwargs):
        """Use the provided name to find the correct implementation,
        then instantiate this implementation.

        """
        return cls[name](*args, **kwargs)

    def __init__(self, config: Config, transport: Transport, **kwargs):
        super().__init__(**kwargs)
        self._config = config
        self._transport = transport

    async def query(self, service: Service) -> Config:
        """Query the current configuration of a container. """

    async def create(self, service: Service, config: Config,
                     name: str = None):
        """Create a container. """

    async def start(self, service: Service, timeout: int = None,
                    force: bool = True):
        """Start a container. """

    async def stop(self, service: Service, timeout: int = None,
                   force: bool = True):
        """Stop a container. """

    async def unmount(self, service: Service, name: str):
        """Detach a container from its name and address, rename it to `old`."""

    async def mount(self, service: Service, new: str, old: str = None):
        """Attach the `new` container to the service's name and address.

        If `old` is provided, automatically unmount any currently attached
        container, renaming it to `old`.

        """

    async def wait(self, service: Service, interval: int = 1000,
                   timeout: int = None):
        """Wait for a container to start up, or to fail.

        Container status is obtained by contacting the `systemd` process running
        in the container.

        """

    async def reboot(self, service: Service,
                     timeout: int = None, expect: int = None,
                     keep_enabled=False):
        """Reboot a container, disabling it until `wait()` completes."""

    async def snapshot(self, service: Service, name: str) -> Snapshot:
        """Save a snapshot of the container, that can be restored on error."""

    async def restore(self, service: Service, snapshot: Snapshot):
        """Restore a container snapshot.

        XXX: we need to purge/promote other snapshots when restoring
             (snapshots made after snapshot() will fail to restore otherwise)

        """

    async def cleanup(self, service: Service, snapshot: Snapshot):
        """Delete a container snapshot.

        """

    async def delete(self, service: Service, timeout: int = None,
                     force: bool = False):
        """Delete a container. """

    async def wipe(self, service: Service):
        """Delete a container and all associated storages. """
