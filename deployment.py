#!/usr/bin/env python3
# pylint: disable=too-few-public-methods,too-many-locals
"""TODO"""
from asyncio import gather
from itertools import chain
from typing import Dict

from .config import Config
from .exceptions import FailedDependency, NodeError
from .manager import DeploymentManager
from .node import Node, NodeNames, NodeSet, Todo
from .service import HostServices
from .utils import AsyncMixin, LogMixin, async_progress, closure, progress

NodeStates = Dict[Node, Config]


class Deployment(AsyncMixin, LogMixin):
    """TODO"""

    def __init__(self, hosts: HostServices, **kwargs):
        self._hosts = hosts
        super().__init__(**kwargs)

    @async_progress("querying state")
    def _query(self) -> NodeStates:
        return gather(*[host.query(svcs) for host, svcs in self._hosts.items()])

    @progress("comparing state")
    def _compare(self, state: NodeStates, rebuild=False) -> Todo:
        to_launch: NodeSet = set()
        to_update: NodeSet = set()
        to_rebuild: NodeSet = set()
        for host, services in self._hosts.items():
            if host.config != state[host]:
                to_update.add(host)
            for service in services:
                if service in state:
                    if state[service] is None:
                        service.log("need to launch")
                        to_launch.add(service)
                    elif service.config != state[service]:
                        if rebuild or service.rebuild or service.backend.rebuild:
                            service.log("need to rebuild")
                            to_rebuild.add(service)
                        else:
                            service.log("need to update")
                            to_update.add(service)
        return to_launch, to_update, to_rebuild

    @async_progress("deploying")
    async def deploy(self, dry_run=False, force_rebuild=False, **kwargs):
        """Deploy the given `hosts` and `services`.

        - `rollback` : set to `False` to disable post-deployment rollbacks
        - `rollback_whole` : set to `True` to rollback the whole deployment
                             (instead of failed services + their dependencies)
        - `snapshot` : set to `False` to disable pre-deployment snapshots
                       (*dangerous*, requires `rollback` to be `False`)
        - `force_rebuild` : force services to be rebuilt, not updated
        - `keep_failed` : set to `True` to keep failed build outputs
                          (by default, everything is deleted at the end)
        - `dry_run` : set to `True` to report changes instead of doing any

        """

        async def _deploy_node(node):
            try:
                await gather(*(tasks[dep] for dep in node.deps))
            except NodeError as error:
                raise FailedDependency(node, error)
            to_launch, to_update, to_rebuild = todo
            if node in to_launch:
                self.log.debug("creating ")
                await node.create(**kwargs)
            elif node in to_update:
                await node.update(**kwargs)
            elif node in to_rebuild:
                new[node] = "{node.name}-{token_hex(8)}"
                old[node] = "{node.name}-{token_hex(8)}"
                await node.create(new[node], **kwargs)
                await node.mount(new[node], old[node], **kwargs)

        kwargs["dry_run"] = dry_run
        state = await self._query()
        todo = self._compare(state, rebuild=force_rebuild)
        order = closure(Node.dependencies(set(chain(*todo))))
        to_launch, to_rebuild, to_update = todo
        new: NodeNames = dict()
        old: NodeNames = dict()
        manager_kwargs = dict(kwargs, dry_run=dry_run, log=self.log)
        manager = DeploymentManager(todo, order, new, old, **manager_kwargs)
        async with manager as (done, errors):
            tasks = {node: _deploy_node(node) for node in order}
            await gather(tasks.values())
