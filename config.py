#!/usr/bin/env python3
"""TODO"""
from typing import ForwardRef, Mapping, TypeVar, Union

# pylint: disable=invalid-name
Key = str
Config = ForwardRef("Config")
Value = Union[str, float, int, bool, Config]
Config = Mapping[Key, Value]

ServiceConfig = Config
HostConfig = Config

Commit = TypeVar("Commit")
Snapshot = TypeVar("Snapshot")
# pylint: enable=invalid-name
