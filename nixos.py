#!/usr/bin/env python3
"""TODO"""
# pylint: disable=too-few-public-methods
from os import path
from secrets import token_hex

from .backend import Backend


class NixosBackend(Backend, type='nixos'):
    """TODO"""
    _target_path = path.join('etc', 'nixos')
    _switch_script = path.join('bin', 'switch-to-configuration')

    async def backup(self):
        # make a backup of the system configuration files
        backup_path = f'{self._target_path}.autonix-{token_hex(8)}'
        command = ['cp', '-rf', self._target_path, backup_path]
        await self._transport.run(command)
        # TODO: add temporary Nix gcroot
        ...
        # save the store path of the current system generation
        command = ['readlink', '/run/current-system']
        current_system = self._transport.run(command)
        return backup_path, current_system

    async def restore(self, snapshot):
        backup_path, current_system = snapshot
        # restore the backup of the system configurtion files
        command = ['mv', '-f', backup_path, self._target_path]
        await self._transport.run(command)
        # switch to the saved system generation
        switch_script = path.join(current_system, self._switch_script)
        command = [switch_script, 'switch']
        await self._transport.run(command)
        # TODO: remove temporary Nix gcroot
        ...

    async def cleanup(self, snapshot):
        backup_path, _ = snapshot
        # restore the backup of the system configurtion files
        command = ['mv', '-f', backup_path, self._target_path]
        await self._transport.run(command)
        # TODO: remove temporary Nix gcroot
        # ...

    async def update(self):
        pass  # TODO: implement NixosBackend.update()
