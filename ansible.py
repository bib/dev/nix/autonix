#!/usr/bin/env python3
"""TODO"""
# pylint: disable=too-few-public-methods
from .backend import Backend


class AnsibleBackend(Backend, type="ansible"):
    """TODO"""

    async def backup(self):
        pass

    async def restore(self, snapshot):
        pass

    async def cleanup(self, snapshot):
        pass

    async def update(self):
        pass
