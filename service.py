#!/usr/bin/env python3
"""This module provides a `Service` class that represents a leaf `Node` of the
infrastructure, and provides service management methods, that are
implemented using the `Hypervisor` and `Backend` classes.

Related modules :
 - `autonix.hypervisor` :: Hypervisor, used to create and manage the
                           containers themselves.
 - `autonix.backend` :: Configuration management backends, used to deploy the
                        system configuration of hosts and services.

WIP state :
 - DONE :: types
 - DONE :: class structure
 - DONE :: method implementations
 - DONE :: docstrings
 - DONE :: linting
 - TODO :: typing check

"""
# pylint: disable=too-many-arguments
from typing import ForwardRef, List, Mapping, Set, TypeVar

from .backend import Backend
from .config import ServiceConfig
from .domain import Domain
from .hypervisor import Hypervisor
from .node import Node
from .utils import async_progress

# pylint: disable=invalid-name
Snapshot = TypeVar("Snapshot")
Host = ForwardRef("Host")

Service = ForwardRef("Service")
Services = List[Service]
ServiceSet = Set[Service]
HostServices = Mapping[Host, ServiceSet]
# pylint: enable=invalid-name


class Service(Node):
    """This class is used to represent managed services, store their runtime
    state, and provide management methods implemented using the provided
    and configuration backends.

    This object should be initialized with a configuration specification
    (`ServiceConfig`), a host (`Host`), and an hypervisor (`Hypervisor`).

    """

    @classmethod
    def from_config(cls, config: ServiceConfig, *args, **kwargs) -> Service:
        """Use the provided configuration to find the correct implementation,
        then instantiate this implementation.

        """
        return cls[config['type']](config, *args, **kwargs)

    def __init__(self, config: ServiceConfig, host: Host, domain: Domain,
                 hypervisor: Hypervisor, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # setup parameters
        self._config = config
        self._host = host
        self._domain = domain
        self._hypervisor = hypervisor
        # setup configuration backend
        self._backend = Backend.from_config(config['backend'], hypervisor)
        # report initialization in debug log
        self.log.debug("initialized client class")

    # Public properties
    # -------------------------------------------------------------------------

    @property
    def name(self) -> str:
        """Name of this service. """
        return self._config["name"]

    @property
    def qualified_name(self) -> str:
        """Fully qualified name of this service. """
        return self._config["qualifiedName"]

    # Service methods
    # -------------------------------------------------------------------------

    @async_progress("creating as {name}")
    async def create(self, name: str = None, dry_run: bool = False) -> str:
        """Create a service, with the given name. """
        if not dry_run:
            await self._hypervisor.create(self, name)

    @async_progress("updating")
    async def update(self, dry_run: bool = False):
        """Update a container's system."""
        if not dry_run:
            await self._backend.update(self)

    @async_progress("starting")
    async def start(self, timeout: int = None, force: bool = True,
                    dry_run: bool = False):
        """Start a service. """
        if not dry_run:
            await self._hypervisor.start(self, timeout=timeout, force=force)

    @async_progress("stoping")
    async def stop(self, timeout: int = None, force: bool = True,
                   dry_run: bool = False):
        """Stop a service. """
        if not dry_run:
            await self._hypervisor.stop(self, timeout=timeout, force=force)

    @async_progress("rebooting")
    async def reboot(self, timeout: int = None, force: bool = False,
                     expect: int = None, keep_enabled=False,
                     dry_run: bool = False):
        """Reboot a service, disabling it until completion. """
        if not keep_enabled:
            disabled = await self.disable(reason="reboot", expect=expect)
        if not dry_run:
            await self._hypervisor.reboot(timeout=timeout, force=force)
        await self.wait(dry_run=dry_run)
        if not keep_enabled and disabled:
            await self.enable()

    @async_progress("waiting")
    async def wait(self, interval: int = 1000, timeout: int = None, dry_run: bool = False):
        """Wait for a service to be running or to fail. """
        if not dry_run:
            await self._hypervisor.wait(self, interval=interval, timeout=timeout)

    @async_progress("unmounting")
    async def unmount(self, name: str, dry_run: bool = False):
        """Unmount the service's container, moving it to the given name."""
        if not dry_run:
            await self._hypervisor.unmount(self, name)

    @async_progress("mounting")
    async def mount(self, name: str, old: str, dry_run: bool = False):
        """Mount the given container name to the service, moving the existing
        container to the given name.

        """
        if not dry_run:
            await self._hypervisor.mount(self, name, old)

    @async_progress("enabling")
    async def enable(self, dry_run: bool = False):
        """Enable a service, making it available from external networks. """
        if not dry_run:
            await self._host.backend.enable(self)

    @async_progress("disabling")
    async def disable(self, reason: str = None, expect: int = None,
                      dry_run: bool = False) -> bool:
        """Disable a service, making it unavailable from external networks. """
        if not dry_run:
            return await self._host.backend.disable(self, reason, expect)

    @async_progress("deleting")
    async def delete(self, timeout: int = None, force: bool = False,
                     dry_run: bool = False):
        """Delete a service (but keep its external storage resources). """
        if not dry_run:
            await self._hypervisor.delete(self, timeout=timeout, force=force)

    @async_progress("wiping")
    async def wipe(self, dry_run: bool = False):
        """Delete a service and all of its external storage resources. """
        if not dry_run:
            await self._hypervisor.wipe(self)
