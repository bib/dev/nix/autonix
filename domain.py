#!/usr/bin/env python3
"""TODO"""
# pylint: disable=too-few-public-methods
from typing import ForwardRef

from .node import Config

# pylint: disable=invalid-name
Host = ForwardRef("Host")
# pylint: enable=invalid-name


class Domain:
    """TODO"""

    def __init__(self, config: Config, host: Host):
        self.config = config
        self.host = host

    @property
    def name(self) -> str:
        """Domain name. """
        return self.config["name"]
