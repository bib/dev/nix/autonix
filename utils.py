#!/usr/bin/env python3
# pylint: disable=too-few-public-methods
"""TODO"""
from abc import ABCMeta
from asyncio import get_running_loop
from concurrent.futures import Executor, ThreadPoolExecutor
from contextlib import contextmanager
from functools import partial, wraps
from inspect import isabstract
from sys import stderr
from typing import Callable, Dict, ForwardRef, List, Tuple, TypeVar

from .node import Node, NodeList, NodeSet

DEBUG = 0
INFO = 30
NOTIFY = 10
WARNING = 40
ERROR = 50

# pylint: disable=invalid-name
File = TypeVar("File")

Log = ForwardRef("Log")
LogLevel = int
LogOutput = Tuple[File, LogLevel]
LogOutputs = List[LogOutput]
# pylint: enable=invalid-name


def closure(graph: Dict[Node, NodeSet]) -> NodeList:
    """Return the transitive order obtained by computing the transitive closure
    of the given graph.

    """
    order: NodeList = []
    while len(order) < len(graph):
        for node, parents in graph.items():
            if all(parent in order for parent in parents):
                order.append(node)
    return order


class PluginMeta(ABCMeta):
    """Metaclass for `PluginBase`, that makes the class behave as a dictionary
    of its implementations.

    """

    def __getitem__(cls, _type):
        name = cls.__name__
        for subcls in cls:
            if subcls._type == _type:
                return subcls
        raise TypeError(f"No '{_type}' implementation found for '{name}'")

    def __iter__(cls):
        for subcls in cls.__subclasses__():
            if not isabstract(subcls):
                if getattr(subcls, "_type", None) is not None:
                    yield subcls


class PluginBase(metaclass=PluginMeta):
    """Base class that provides a simple plugin system where subclasses can
    register implementation types, and callers can get a specific
    implementation.

    """

    def __init_subclass__(cls, _type=None, **kwargs):
        super().__init_subclass__(**kwargs)
        cls._type = _type


class AsyncMixin:
    """Asynchronous mixin that provides a `loop` attribute with the event loop
    running at initialization time, and an `executor` attribute with a
    single-worker executor.

    Can be used with the `@async_background` decorator to schedule methods on
    instance executors.

    """

    def __init__(self, *args, executor: Executor = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.executor = executor or ThreadPoolExecutor(max_workers=1)

    def _run_async(self, task, *args, **kwargs):
        func = partial(task, *args, **kwargs)
        return get_running_loop().run_in_executor(self.executor, func)

    @classmethod
    def _async_background(cls, text, runner):
        def decorator(func):
            @wraps(func)
            async def wrapper(self, *args, **kwargs):
                if not isinstance(self, cls):
                    raise TypeError("async_background() should be used "
                                    " on AsyncMixin instances.")
                task = partial(func, self, *args, **kwargs)
                return await getattr(self, runner)(task)

            if text is not None:
                wrapper = async_progress(text)(wrapper)
            return wrapper

        return decorator


class Log:
    """TODO"""

    def __init__(self, name, outputs: LogOutputs = (stderr, INFO),
                 parent: Log = None):
        self.name = name
        self.outputs = outputs
        self.parent = parent
        self._written = 0
        self._dirty = False

    def write(self, text, level=DEBUG, **kwargs):
        """TODO"""
        if self.parent is not None:
            self.parent.write(text, level=level, **kwargs)
        self._written += 1
        for output, output_level in self.outputs:
            if level <= output_level:
                print(text, file=output, **kwargs)
        self._dirty = False

    def _write_resume(self, level: int, header: str,
                      index: int, text: str, **kwargs):
        if index != self._written:
            self.write(header, level=level, end="", **kwargs)
        self.write(text, level=level, **kwargs)

    def write_partial(self, text: str, level=DEBUG, **kwargs) -> Callable[str]:
        """TODO"""
        header = f"{text}... "
        self.write(header, level=level, end="", **kwargs)
        self._dirty = True
        index = self._written
        return partial(self._write_resume, level, header, index, **kwargs)

    @contextmanager
    def progress(self, text, level=NOTIFY):
        """TODO"""
        resume = self.write_partial(text, level=level)
        try:
            yield
        except Exception as ex:
            resume("failed ({type(ex).__name__}).")
            raise ex
        else:
            resume("done.")

    def debug(self, text, **kwargs):
        """TODO"""
        self.write(f"  DEBUG [{self.name}] {text}", level=DEBUG, **kwargs)

    def info(self, text, **kwargs):
        """TODO"""
        self.write(f"   INFO [{self.name}] {text}", level=INFO, **kwargs)

    def notify(self, text, **kwargs):
        """TODO"""
        self.write(f" NOTIFY [{self.name}] {text}", level=NOTIFY, **kwargs)

    def warning(self, text, **kwargs):
        """TODO"""
        self.write(f"WARNING [{self.name}] {text}", level=WARNING, **kwargs)

    def error(self, text, **kwargs):
        """TODO"""
        self.write(f"  ERROR [{self.name}] {text}", level=ERROR, **kwargs)


class LogMixin:
    """Mixin that provides a `log` attribute with an instance-specific logger.

    """

    def __init__(self, *args, log: Log = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.log = self._make_logger(log)

    @property
    def _logger_type(self) -> str:
        return type(self).__name__

    @property
    def _logger_name(self) -> str:
        return '{self._logger_type}:{hex(id(self))}'

    def _make_logger(self, parent: Log = None):
        return Log(self._logger_name, parent=parent)


def progress(text: str, logger: str = "log"):
    """Method decorator that uses the instance's logger to show
    entry/exit messages.

    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            with getattr(self, logger).progress(text):
                return func(self, *args, **kwargs)

        return wrapper

    return decorator


def async_progress(text: str, logger: str = "log"):
    """Method decorator that uses the instance's logger to show
    entry/exit messages.

    """

    def decorator(func):
        @wraps(func)
        async def wrapper(self, *args, **kwargs):
            with getattr(self, logger).progress(text):
                return await func(self, *args, **kwargs)

        return wrapper

    return decorator


def async_background(text: str = None, runner: str = "_run_async"):
    """Blocking method decorator that uses an instance's method to
    asynchronously schedule the method on another thread.


    """
    # pylint: disable=protected-access
    return AsyncMixin._async_background(text, runner)
