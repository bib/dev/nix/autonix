#!/usr/bin/env python
"""TODO"""
# pylint: disable=too-few-public-methods,too-many-arguments

from sys import argv
from .cli import CLI


def main():
    """Main entry point. """
    CLI()(argv)


if __name__ == '__main__':
    main()
