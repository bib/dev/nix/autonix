{ config, lib, pkgs, ... }:
with builtins;
with lib;
let

  cfg = config.deployment;

  # Build host-level systemd interface definitions for the given services
  serviceInterfaces = mapAttrs' (name: service: {
    name = "lxd-${name}";
    value = {
      name = "lxd-${name}";
      addresses = [{
        addressConfig = {
          Address = service.host.address.internal;
          Peer = service.address;
        };
      }];
    };
  });

  # Build global address/hostname mapping (for /etc/hosts)
  serviceNames = environment:
    concatStringsSep "\n" (flatten (mapAttrsToList (name: host:
      mapAttrsToList
      (name: service: "${service.address} ${service.qualifiedName}")
      host.services) environment));

in {
  systemd.network.networks = serviceInterfaces deployment.services;
  networking.extraHosts = serviceNames deployment.services;
}
