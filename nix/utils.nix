{ lib }:
with builtins;
with lib;

rec {
  # Common predicates
  nonEmpty = x: length x > 0;
  nonEmptyAttrs = x: isAttrs x && nonEmpty (attrNames x);
  nonEmptyList = x: isList x && nonEmpty x;

  # Recursively apply predicate, exluding values that were already seen
  #   NOTE: adapted from standard library, adding the `seen` stack
  #   NOTE: could be improved by finding a way to mark-then-drop the 
  #         recursive attributes, so that we don't have to keep
  #         `seen` while evaluating other attributes
  #   NOTE: in current usage, graph is around 10 levels deep
  filterAttrsCyclic = filterAttrsCyclic' [ ];
  filterAttrsCyclic' = seen: pred: set:
    listToAttrs (concatMap (name:
      let v = set.${name};
      in if !elem v seen && pred v then
        [
          (nameValuePair name (let s = seen ++ [ v ];
          in if isAttrs v then
            filterAttrsCyclic' s pred v
          else if isList v then
            filterListCyclic' s pred v
          else
            v))
        ]
      else
        [ ]) (filter (x: x != "_module") (attrNames set)));

  # Recursively apply predicate, exluding elements that were already seen
  filterListCyclic = filterListCyclic' [ ];
  filterListCyclic' = seen: pred: list:
    concatLists (map (v:
      if !elem v seen && pred v then
        [
          (let s = seen ++ [ v ];
          in if isAttrs v then
            filterAttrsCyclic' s pred v
          else if isList v then
            filterListCyclic' s pred v
          else
            v)
        ]
      else
        [ ]) list);

  # Concatenate a list of attrsets
  concatAttrsCheck = check:
    zipAttrsWith (name: value: elemAt (check name value) 0);

  # Try to evaluate the given attributes, only keeping the ones that don't fail
  tryAttrs = filterAttrs (name: value: (tryEval value).success);

  # Recursively clean attributes to prepare for JSON export
  #   keeps only string, int, bool, non-empty attrsets and lists
  #   removes cyclic values (already seen) from attrsets and lists
  cleanAttrs = x:
    (filterAttrsCyclic (v:
      isString v || isInt v || isBool v || nonEmptyAttrs v || nonEmptyList v)
      x);
}
