# TODO:
#  - [ ] integrate with cloud-init images for container pre-configuration
#  - [X] add fixed-point evaluation for host/domain/service options
#    - allow hosts and domains to provide default values for service definitions
#    - allow the default values to depend on the current host/domain/service
#  - [ ] make definitions agnostic of the virtualization method
#    + [X] move LXD definitions to their own namespace
#    + [ ] add support for nsspawn/machinectl LXC guests
#    + [ ] add support for lightweight KVM guests
#    + [ ] add support for Docker/podman guests
# -  [ ] complete LXD support
#    + [X] add network interface definitions
#    + [ ] add hostname definitions
#    + [ ] add container configuration generators
# -  [ ] prepare secret deployment
# -  [ ] add configuration script generator (using Python module)
#    + [X] add JSON export to provide the configuration to the script
#    + [X] provide a way to check configuration equality (canonicalized JSON)
#
# FIXME:
# - [ ] attempt some performance improvements
# - [ ] maybe use something better than JSON canonicalization
#
# NOTE:
# - at some point, this module (and related files) should probably be moved to
#   its own repository
# - it could be *much* more performant to only do a single fix-point evaluation when
#   applying the host/domain/service tree
# - some work should be done on host-level options
#   + why should something be here and not in hosts/$host/*.nix ?
# - some work should be done on addresses handling
# - no work at all has been done about screts

{ config, lib, ... }:
with builtins;
with lib;
with types;
with (import ./deploy/utils.nix { inherit lib; });

let

  cfg = config.deployment;

  # Function definitions
  # ===========================================================================

  # LXD option generation
  lxdOptions = let
    lxdKey = path: concatStringsSep "." (filter nonEmpty ([ prefix ] ++ path));
    lxdOption = path: value: (lxdKey prefix path) + " = " + (toJSON value);
  in prefix: values: collect isString (mapAttrsRecursive (lxdOption values));

  getHostServices = host: host.services;
  getDomainServices = domain: domain.services;

  # Check that the given services are only defined in a single domain.
  checkServices = name: services:
    let
      domainName = service: service.domain.name;
      domainNames = concatMapStringsSep ", " domainName services;
    in if length services > 1 then
      throw "Conflicting definitions for service ${name} (in ${domainNames})."
    else
      services;

  # Get a list of all the services of the given domains
  mapServices = domains:
    concatAttrsCheck checkServices (map getDomainServices domains);

  # Get a list of all the domain names of the given services
  getDomains = service: service.domains;
  collectDomains = services: flatten (map getDomains (attrValues services));

  #

  # Evaluate a service definition, for given host and domain
  applyService = host: domain: name: input:
    fix (service:
      recursiveUpdate ((cfg.serviceDefaults host domain name service)
        // (host.serviceDefaults domain name service)
        // (domain.serviceDefaults name service)) (tryAttrs input));

  # Evaluate a domain definition, for a given host
  applyDomain = host: name: input:
    fix (domain:
      input // (cfg.domainDefaults host name domain)
      // (host.domainDefaults name domain) // (tryAttrs input) // {
        services = mapAttrs (applyService host domain) input.services;
        names = collectDomains domain.services;
      });

  # Evaluate a host definition
  applyHost = name: input:
    fix (host:
      input // (cfg.hostDefaults name host) // (tryAttrs input) // {
        services = mapServices (attrValues host.domains);
        domains = mapAttrs (applyDomain host) input.domains;
        environment = cfg.environment.hosts."${host.name}";
        address = host.environment.address;
      });

  # Option types definitions
  # ===========================================================================

  # Administration
  # ---------------------------------------------------------------------------

  adminOptionType = { name, ... }: {
    options = {
      name = mkOption {
        default = name;
        type = str;
        description = "Administrator username.";
      };
      contact = mkOption {
        default = null;
        type = nullOr str;
        description = "Contact address.";
      };
      ssh_keys = mkOption {
        default = [ ];
        type = listOf str;
        description = "SSH public keys.";
      };
    };
  };

  # Certificate
  # ---------------------------------------------------------------------------

  certificateOptionType = {
    options = {
      wildcard = mkOption {
        default = false;
        type = bool;
        description = "Obtain certificates matching all subdomains.";
      };
      webroot = mkOption {
        default = false;
        type = bool;
        description = "Use webroot challenge.";
      };
      dnsProvider = mkOption {
        default = null;
        type = nullOr str;
        description = "API provider for DNS-01 challenge.";
      };
      credentialsFile = mkOption {
        default = null;
        type = nullOr str;
        description = "API credentials file.";
      };
    };
  };

  # Backend option types
  # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  # HTTP backends
  # ---------------------------------------------------------------------------

  httpOptionType = {
    options = {
      enable = mkEnableOption "HTTP backend";
      options = mkOption {
        default = { };
        type = attrs;
        description = "HAProxy backend options.";
      };
      timeout = mkOption {
        default = { };
        type = attrs;
        description = "HAProxy backend timeouts.";
      };
    };
  };

  # SMTP/IMAP backends
  # ---------------------------------------------------------------------------

  smtpOptionType = { options = { enable = mkEnableOption "SMTP backend"; }; };

  imapOptionType = { options = { enable = mkEnableOption "IMAP backend"; }; };

  # TCP backends
  # ---------------------------------------------------------------------------

  tcpOptionType = {
    options = {
      enable = mkEnableOption "Raw TCP backend";
      proxyProtocol = mkOption {
        type = bool;
        default = false;
        description = "Send PROXY protocol header to backend.";
      };
      sni = mkOption {
        type = bool;
        default = true;
      };
      ports = mkOption {
        default = { };
        type = attrsOf int;
        description = "Frontend/backend port mapping.";
      };
    };
  };

  sslTcpOptionType = {
    options = {
      enable = mkEnableOption "SSL TCP backend";
      sni = mkOption {
        type = bool;
        default = true;
        description = "Enable SNI (Server Name Indication).";
      };
      proxyProtocol = mkOption {
        type = bool;
        default = false;
        description = "Send PROXY protocol header to backend.";
      };
      ports = mkOption {
        default = { };
        type = attrsOf int;
        description = "Frontend/backend port mapping.";
      };
    };
  };

  # NAT backend
  # ---------------------------------------------------------------------------
  # TODO: type

  natOptionType = {
    options = {
      enable = mkEnableOption "HTTP backend";
      rules = mkOption {
        default = [ ];
        # type = list;
        description = "NAT firewall rules";
      };
    };
  };

  # Backend options
  # ---------------------------------------------------------------------------

  backendOptionType = {
    options = {
      # HTTP backend options (through haproxy)
      http = mkOption {
        type = submodule httpOptionType;
        default = { };
        description = "HTTP backend options.";
      };
      # SMTP backend options (through nginx)
      smtp = mkOption {
        type = submodule smtpOptionType;
        default = { };
        description = "SMTP backend options.";
      };
      # IMAP backend options (through nginx)
      imap = mkOption {
        type = submodule imapOptionType;
        default = { };
        description = "IMAP backend options.";
      };
      # Raw TCP backend options (through haproxy)
      tcp = mkOption {
        type = submodule tcpOptionType;
        default = { };
        description = "Raw TCP backend options.";
      };
      # SSL TCP backend options (through haproxy)
      sslTcp = mkOption {
        type = submodule sslTcpOptionType;
        default = { };
        description = "SSL TCP backend options.";
      };
      # NAT backend options (through nftables)
      nat = mkOption {
        type = submodule natOptionType;
        default = { };
        description = "NAT backend options.";
      };
    };
  };

  lxdOptionType = {
    options = {
      # LXD storage options
      storages = mkOption {
        type = attrs;
        default = { };
        description = "Storage options.";
      };
      # LXD network resource limits
      interfaces = mkOption {
        type = attrs;
        default = { };
        description = "Networking limits.";
      };
      # LXD generic resource limits
      limits = mkOption {
        type = attrs;
        default = { };
        description = "Resource limits.";
      };
      # LXD security configuration
      security = mkOption {
        type = attrs;
        default = { };
        description = "Security options.";
      };
    };
  };

  checkOptionType = {
    options = {
      # Name
      name = mkOption {
        type = str;
        description = "Check name.";
      };
      # LXD network resource limits
      script = mkOption {
        type = str;
        description = "Check script.";
      };
    };
  };

  # Host/domain/service option types
  # ---------------------------------------------------------------------------

  hostOptionType = { name, ... }: {
    options = {
      # Name
      name = mkOption {
        default = name;
        type = str;
        description = "Name of this host.";
      };
      # Qualified name
      qualifiedName = mkOption {
        type = nullOr str;
        description = "Fully qualified name of this host.";
      };
      # Default values for domains
      domainDefaults = mkOption {
        default = name: domain: { };
        description = "Default options for the domains of this host.";
      };
      # Default values for services
      serviceDefaults = mkOption {
        default = domain: name: service: { };
        description = "Default options for the services of this host.";
      };
      # Addresses
      address = mkOption {
        type = attrs;
        description = "Addresses.";
      };
      # Domain mapping
      domains = mkOption {
        default = { };
        type = attrsOf (submodule (domainOptionType name));
        description = "Service domain definitions.";
      };
      # Generated service mapping
      services = mkOption {
        internal = true;
        default = { };
        type = attrsOf (submodule serviceOptionType);
        description = "Services of this host, for all domains.";
      };
      # Host definition in current environment
      environment = mkOption {
        internal = true;
        type = attrs;
        description = "Host definition of the current environment.";
      };
    };
  };

  domainOptionType = hostName:
    { name, ... }: {
      options = {
        # Name
        name = mkOption {
          default = name;
          type = str;
          description = "Name of this domain.";
        };
        serviceDefaults = mkOption {
          default = name: backend: { };
          description = "Default options for the services of this host.";
        };
        # ACME certificate options
        certificate = mkOption {
          default = { };
          type = attrs;
          description = "ACME renewal options (see security.acme.certs.*).";
        };
        # Simple services option
        services = mkOption {
          default = { };
          type = attrsOf (submodule serviceOptionType);
          description = "Service definitions.";
        };
        # Domain reference
        names = mkOption {
          internal = true;
          type = listOf str;
          description = "Child domain names.";
        };
        # Domain reference
        host = mkOption {
          internal = true;
          default = null;
          type = submodule hostOptionType;
          apply = foo: getAttr hostName cfg.hosts;
          description = "Parent host reference.";
        };
      };
    };

  serviceOptionType = { name, ... }: {
    options = {
      disable = mkOption {
        default = true;
        type = bool;
        description = "Disable service (no container creation, no forwarding)";
      };
      # Service name
      name = mkOption {
        default = name;
        type = str;
        description = "Name of this service (container hostname).";
      };
      # Qualified name
      qualifiedName = mkOption {
        type = nullOr str;
        description = "Fully qualified name of this service.";
      };
      # Backend IP address
      address = mkOption {
        type = str;
        description = "IPv4 address of this service's backend.";
      };
      # Service domains
      domains = mkOption {
        default = [ ];
        type = listOf str;
        description = ''
          Domains to match for this service's frontend.

                  Used for :
                   - DNS records configuration
                   - SSL certificate generation
                   - reverse proxy backend mapping
                '';
      };
      # LXD storage configuration
      lxd = mkOption {
        type = submodule lxdOptionType;
        default = { };
        description = "LXD options.";
      };
      # Backends
      backends = mkOption {
        type = submodule backendOptionType;
        default = { };
        description = "Backend options.";
      };
      # Dependencies
      deps = mkOption {
        type = listOf attrs;
        default = [ ];
        description = "List of services that this service depends on.";
        apply = map (dep: {
          name = dep.name;
          qualifiedName = dep.qualifiedName;
          domainName = dep.domain.name;
          hostName = dep.host.name;
        });
      };
      # Run-time checks
      checks = mkOption {
        type = listOf (submodule checkOptionType);
        default = [ ];
        description = "Run-time checks.";
      };
      # NixOps container configuration
      # TODO: write nixosOptionType
      nixos = mkOption {
        type = attrs;
        default = { };
        description = "NixOS container options.";
      };
      # Domain reference
      domain = mkOption {
        internal = true;
        type = submodule (domainOptionType name);
        description = "Parent domain reference.";
      };
      # Domain reference
      host = mkOption {
        internal = true;
        type = submodule hostOptionType;
        description = "Parent host reference.";
      };
    };
  };

in {

  # Configuration values
  # ===========================================================================

  config = { };

  # Option declarations
  # ===========================================================================

  options = {

    deployment.final = mkOption {
      default = null;
      internal = true;
      apply = _: cleanAttrs cfg.hosts;
    };

    # Integration options
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    security.acme.webroot = mkOption {
      default = "/var/www/";
      type = str;
      description = "Default webroot path for ACME web challenges.";
    };

    # Administrators
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.publicContact = mkOption {
      type = str;
      description = "TODO";
    };

    deployment.adminContact = mkOption {
      type = str;
      description = "TODO";
    };

    deployment.hostAdmins = mkOption {
      type = attrsOf (submodule adminOptionType);
      description = "TODO";
    };

    deployment.serviceAdmins = mkOption {
      type = attrsOf (listOf (submodule adminOptionType));
      description = "TODO";
    };

    # Environments
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    # TODO: write complete type

    deployment.environment = mkOption {
      default = { };
      type = attrs;
      description = "TODO";
    };
    deployment.environments = mkOption {
      default = { };
      type = attrs;
      description = "TODO";
    };

    # SSL certificates
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.certificates = mkOption {
      default = { };
      type = attrsOf (submodule certificateOptionType);
      description = "SSL certificates configuration";
    };

    # Default options
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.hostDefaults = mkOption {
      default = name: host: { };
      description = "Default options for all hosts.";
    };

    deployment.domainDefaults = mkOption {
      default = name: domain: { };
      description = "Default options for all domains.";
    };

    deployment.serviceDefaults = mkOption {
      default = name: service: { };
      description = "Default options for all services.";
    };

    # Host declarations
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.hosts = mkOption {
      default = { };
      type = attrsOf (submodule hostOptionType);
      description = "Hosts to be deployed.";
      apply = mapAttrs applyHost;
    };

    deployment.host = mkOption {
      default = { };
      internal = true;
      type = submodule hostOptionType;
      description = "TODO";
    };
  };
}
