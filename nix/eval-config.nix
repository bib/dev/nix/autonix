{ configuration ? import <nixpkgs/nixos/lib/from-env.nix> "CONFIG" <config>
, system ? builtins.currentSystem }:

let

  eval = import <nixpkgs/nixos/lib/eval-config.nix> {
    inherit system;
    modules = [ configuration ];
    baseModules = [
      <nixpkgs/nixos/modules/misc/nixpkgs.nix>
      <nixpkgs/nixos/modules/misc/assertions.nix>
      <autonix/options.nix>
    ];
  };

in {
  inherit (eval) pkgs config options;
  system = eval.config.system.build.toplevel;
}
