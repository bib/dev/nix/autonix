#!/usr/bin/env python3
"""TODO"""
import argparse
# pylint: disable=too-few-public-methods,too-many-arguments
import json
import os.path
import sys


def print_err(*args, **kwargs) -> int:
    """Wrap print() to write to the error output. """
    print(*args, file=sys.stderr, **kwargs)


def indent_formatter(*args, **kwargs):
    """Wrap RawTextHelpFormatter to set max_help_position. """
    return argparse.RawTextHelpFormatter(*args, max_help_position=48, **kwargs)


class PathAction(argparse.Action):
    """TODO"""

    def __call__(self, parser, args, values, option_string=None):
        try:
            path = os.path.abspath(values)
            with open(path) as _:
                pass
        except FileNotFoundError:
            print_err("file not found: ", path)
            sys.exit(2)
        except PermissionError:
            print_err("file not readable: ", path)
            sys.exit(2)
        setattr(args, self.dest, path)


DESCRIPTION = """AutoNix deployment tool"""
STATUS_DESCRIPTION = "Show deployment status"
LIST_DESCRIPTION = "List configured services"
INFO_DESCRIPTION = "Show service metadata"
WAIT_DESCRIPTION = "Wait for services to be ready"
DEPLOY_DESCRIPTION = "Deploy configured services"
ENABLE_DESCRIPTION = "Enable service (block remote access)"
DISABLE_DESCRIPTION = "Disable service (allow remote access)"
REBOOT_DESCRIPTION = "Reboot container"
START_DESCRIPTION = "Start container"
STOP_DESCRIPTION = "Stop container"
WIPE_DESCRIPTION = "Wipe container"

PROG = "autonix"

HEADER = """Build, deploy and manages the hosts, domains and services
configured in the provided deployment definition, as defined by the
NixOS module.

This script requires :
 - the Nix package manager, and a nixpkgs channel
 - the Python 3 interpreter
 - the OpenSSH client
 - the private key of at least one of the administrator public keys

This script does not store any kind of local state, and should make no
modification to the current system, other than building Nix derivations when
using local NixOS builds for hosts or services. The ability to build derivations
is not required for remote builds.

"""


def _make_parser(parsers, name, **kwargs):
    parser = parsers.add_parser(name, add_help=False, **kwargs)
    parser.add_argument(
        "-h", "--help", action="help", help="Show this help message",
    )
    return parser


def _add_arguments(parser):
    parser.add_argument(
        "-h", "--help", action="help", help="Show this help message",
    )
    parser.add_argument(
        "-c",
        "--config",
        metavar="CONFIG",
        default="./services.nix",
        action=PathAction,
        help="Nix deployment specification.",
    )
    parser.add_argument(
        "-H",
        "--host",
        default=[],
        metavar="HOST",
        nargs=1,
        action="extend",
        dest="hosts",
        help="Filter hosts",
    )
    parser.add_argument(
        "-D",
        "--domain",
        default=[],
        metavar="DOMAIN",
        nargs=1,
        action="extend",
        dest="domains",
        help="Filter domains",
    )
    parser.add_argument(
        "-S",
        "--service",
        default=[],
        metavar="SERVICE",
        nargs=1,
        action="extend",
        dest="services",
        help="Filter services",
    )
    parser.add_argument(
        "-o",
        "--option",
        default=[],
        metavar=("KEY", "VALUE"),
        nargs=2,
        action="append",
        dest="nix_options",
        help="Nix evaluation options",
    )
    parser.add_argument(
        "-I",
        "--include",
        default=[],
        metavar="PATH",
        nargs=1,
        action="extend",
        dest="nix_path",
        help="Nix expression search path",
    )
    parser.add_argument(
        "-n", "--dry-run", action="store_true", help="Don't make any changes"
    )
    parser.add_argument(
        "-v", "--verbose", action="count", default=0, help="Be more verbose"
    )
    parser.add_argument("-d", "--debug", action="store_true", help="Debugging mode")


def _add_commands(parser):
    parsers = parser.add_subparsers(required=True, metavar="COMMAND", dest="command",)

    # Status command
    # -------------------------------------------------------------------------

    status_parser = _make_parser(parsers, "status", help=STATUS_DESCRIPTION)
    status_parser.add_argument(
        "-r", "--running", action="store_true", help="Only show running hosts/services"
    )
    status_parser.add_argument(
        "-f",
        "--format",
        type=str,
        metavar="FORMAT",
        choices=["text", "table", "csv", "json"],
        help="Output format (table/csv/json)",
    )
    status_parser.add_argument(
        "-i", "--interval", type=int, default=1, help="Watch interval (in seconds)",
    )
    status_parser.add_argument(
        "-w", "--watch", action="store_true", help="Continuous output"
    )

    # List command
    # -------------------------------------------------------------------------

    list_parser = _make_parser(parsers, "list", help=LIST_DESCRIPTION)
    list_parser.add_argument(
        "-f",
        "--format",
        type=str,
        metavar="FORMAT",
        choices=["table", "csv", "json"],
        help="Output format (table/csv/json)",
    )

    # Info command
    # -------------------------------------------------------------------------

    info_parser = _make_parser(parsers, "info", help=INFO_DESCRIPTION)
    info_parser.add_argument(
        "-f",
        "--format",
        type=str,
        metavar="FORMAT",
        choices=["text", "table", "csv", "json"],
        help="Output format (table/csv/json)",
    )
    group = info_parser.add_mutually_exclusive_group()
    group.add_argument(
        "service", metavar="SERVICE", nargs="?", help="Show informations of service"
    )
    group.add_argument(
        "host", metavar="HOST", nargs="?", help="Show informations of host"
    )

    # Wait command
    # -------------------------------------------------------------------------

    wait_parser = _make_parser(parsers, "wait", help=WAIT_DESCRIPTION)
    wait_parser.add_argument("--timeout", type=int, help="Maximum time to wait")

    # Deploy command
    # -------------------------------------------------------------------------

    deploy_parser = _make_parser(parsers, "deploy", help=DEPLOY_DESCRIPTION)
    group = deploy_parser.add_mutually_exclusive_group()
    group.add_argument("-l", "--local", action="store_true", help="Force local build")
    group.add_argument("-r", "--remote", action="store_true", help="Force remote build")
    deploy_parser.add_argument(
        "environment", metavar="ENV", type=str, help="Environment to deplay",
    )
    deploy_parser.add_argument(
        "-j", "--tasks", type=int, default=0, help="Maximum concurrent tasks"
    )

    # Reboot command
    # -------------------------------------------------------------------------

    reboot_parser = _make_parser(parsers, "reboot", help=REBOOT_DESCRIPTION)

    # Wipe command
    # -------------------------------------------------------------------------

    wipe_parser = _make_parser(parsers, "wipe", help=WIPE_DESCRIPTION)
    wipe_parser.add_argument(
        "-f", "--force", action="store_true", help="Wipe running containers"
    )

    # Reboot command
    # -------------------------------------------------------------------------

    reboot_parser = _make_parser(parsers, "reboot", help=REBOOT_DESCRIPTION)
    reboot_parser.add_argument("-t", "--timeout", type=int, help="Maximum time to wait")

    # Start/stop commands
    # -------------------------------------------------------------------------

    _make_parser(parsers, "start", help=START_DESCRIPTION)
    stop_parser = _make_parser(parsers, "stop", help=STOP_DESCRIPTION)
    stop_parser.add_argument("-t", "--timeout", type=int, help="Maximum time to wait")

    # Enable/disable command
    # -------------------------------------------------------------------------

    _make_parser(parsers, "enable", help=ENABLE_DESCRIPTION)
    disable_parser = _make_parser(parsers, "disable", help=DISABLE_DESCRIPTION)
    disable_parser.add_argument(
        "-m/--message", type=int, help="Message to show on the error page"
    )


class CLI:
    """TODO"""

    def __init__(self, spec=None):
        self.spec = spec
        self.parser = argparse.ArgumentParser(
            prog=PROG,
            description=HEADER,
            add_help=False,
            formatter_class=indent_formatter,
        )
        self.subparsers = []
        _add_arguments(self.parser)
        _add_commands(self.parser)

    def __call__(self, argv):
        """Separate arguments and feed them to the global/command parsers."""
        if len(argv) == 1:
            self.parser.print_usage()
            sys.exit(2)
        args = self.parser.parse_args(argv[1:])
        if args.spec is None:
            args.spec = json.load(sys.stdin)
        return args


def main():
    """Main entry point. """
    cli = CLI()
    print(json.dumps(cli(sys.argv).__dict__))


if __name__ == "__main__":
    main()
