#!/usr/bin/env python3
# pylint: disable=too-many-instance-attributes,too-many-arguments
"""This modules provides a context manager that abstracts snapshots and
rollbacks from the Deployment class.

"""
from asyncio import gather
from typing import Tuple

from .exceptions import DeploymentError, NodeError, NodeErrors, RollbackError
from .node import Node, NodeList, NodeNames, NodeSet, NodeSnapshots, Todo
from .utils import Log, closure


class DeploymentManager:
    """This class provides a context manager than handles automatic snapshots
    upon entry and rollbacks on errors upon exit, so that this complexity can
    be abstracted away from the `Deployment` class.

    The code is split in 4 methods :
     - `__enter__` : Entry point, make snapshots and setup result values
     - `__exit__` : Exit point, handle errors by calling `_rollback_deployment`
     - `_rollback_deployment` : Calling `_rollback_node` for each node
     - `_rollback_node` : Rollback a single node

    """

    def __init__(self, todo: Todo, order: NodeList,
                 new: NodeNames, old: NodeNames,
                 log: Log = None, dry_run: bool = False, keep_failed: bool = True,
                 rollback: bool = True, rollback_whole: bool = False):
        # save parameters
        self.log, self._todo = log, todo
        self._to_launch, self._to_update, self._to_rebuild = todo
        self._order, self._old, self._new = order, old, new
        self._rollback, self._rollback_whole = rollback, rollback_whole
        self._keep_failed, self._dry_run = keep_failed, dry_run
        # setup state
        self._done: NodeSet = set()
        self._errors: NodeErrors = dict()
        self._restored: NodeSet = set()
        self._snapshots: NodeSnapshots = dict()

    async def __aenter__(self) -> Tuple[NodeSet, NodeErrors]:
        for node in self._to_update:
            self._snapshots[node] = await node.snapshot(dry_run=self._dry_run)
        await gather(*map(self._snapshot_node, self._to_update))
        return self._done, self._errors

    async def __aexit__(self, _, __, ___) -> bool:
        done, dep_errors = self._done, self._errors
        if self._errors:
            text = f"{len(dep_errors)} errors, {len(done)} done"
            self.log.error(f"deployment failed ({text})", errors=dep_errors)
            if self._rollback:
                restored, errors = self._rollback_deployment()
                if errors:
                    text = f"{len(errors)} errors, {len(restored)} restored"
                    self.log.error(f"rollback failed ({text})", errors=errors)
                    raise RollbackError(dep_errors, done, errors, restored)
                text = f"{len(restored)} nodes restored"
                self.log.info(f"rollback completely successfully ({text})")
            raise DeploymentError(dep_errors, done)
        details = f"{len(done)} nodes deployed"
        self.log.info(f"deployment completely successfully ({details})")
        with self.log.progress("clearing snapshots"):
            for node, snapshot in self._snapshots.items():
                node.clear(snapshot, dry_run=self._dry_run)
        return False

    async def _snapshot_node(self, node: Node):
        """Snapshot a single node, saving its """
        self._snapshots[node] = await node.snapshot(dry_run=self._dry_run)

    async def _rollback_node(self, node: Node):
        """Rollback a single node, by undoing the deployment order (delete
        launched containers, rollback updated containers, restore rebuilt
        containers).

        """
        if node in self._to_launch:
            if self._keep_failed:
                await node.unmount(dry_run=self._dry_run)
            else:
                await node.delete(dry_run=self._dry_run)
        elif node in self._to_update:
            await node.rollback(
                self._snapshots[node], keep_failed=self._keep_failed, dry_run=self._dry_run
            )
        elif node in self._to_rebuild:
            if self._keep_failed:
                await node.unmount(dry_run=self._dry_run)
            await node.mount(self._old[node], force=True, dry_run=self._dry_run)

    async def _rollback_deployment(self) -> Tuple[NodeSet, NodeErrors]:
        """Attempt to rollback the failed services in the given deployment
        orders, returning an error mapping of services that failed to rollback,
        and a set of rolled back services .

        """
        rollback_errors: NodeErrors = dict()
        rolled_back: NodeSet = set()

        # determine new ordering if not restoring everything
        if not self._rollback_whole:
            order = closure(Node.dependencies(self._errors.keys()))
            self.log.warn("rolling back failed services")
        else:
            self.log.warn("rolling back entire deployment")
        # rollback each node in transitive closure order
        for node in order:
            with node.log.progress("rolling back"):
                try:
                    await self._rollback_node(node)
                except NodeError as error:
                    rollback_errors[node] = error / 0
                else:
                    rolled_back.add(node)
        return rolled_back, rollback_errors
