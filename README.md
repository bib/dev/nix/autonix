# AutoNix -- Nix-based deployment system

AutoNix is composed of a Python tool and an associated NixOS module, that allows to configure and automate the deployment of NixOS hosts and service containers. The NixOS module provides the options that will be configured to define the deployment's hosts, domains and services, and that will be evaluated by the Python management tool. Using the result of this evaluation, the management tool can then deploy the resources and secrets to the remote hosts, build and activate their new configuration, create the service containers, and deploy the service in these containers.

## Features
 
  - no state is kept on the client
  - configuration is specified using the Nix language
  - configuration of hosts, domains and services
  - automatic DNS and SSL certificate management
  - TCP/HTTP/SMTP/IMAP reverse proxying (or NAT) to services
  - service containers are currently handled by LXD, other backends are planned
  - rollback is quick and easy (both LXD containers and NixOS systems can be rolled back)
  - if necessary, rollback can be configured to rollback the service data as well

## Planned features

 - integration of monitoring system configuration
 - integration of container backends (OCI/Docker/podman/Firecracker/etc)
 - integration of other config management tools (Ansible)
 - N+1 services, spanning multiple hosts
 - run-time migration of services from one host to the other host
 - declarative host provisioning
 - vulnerability tracking
 
## WIP
 
 - implement Host/Service query() methods
 - move address configuration from services.nix to options.nix
 - add transports
   + add class in host, make them used for remote operations
   + use host transports for LXD API client
   + specify transport configuration in options.nix
 - add hypervisor backends
   + will have to support mupltiple backend per host
   + only LXD support will be implemented for now
   + will have to support Firecracker and Docker-based solutions
 - add configuration backends
   + base backend is NixOS, both for hosts and services
   + will have to support raw deployment scripts
   + will have to support Ansible playbooks
 - implement CLI commands
