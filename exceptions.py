#!/usr/bin/env python3
"""TODO"""
from typing import Dict

from .node import Node, NodeSet

NodeErrors = Dict[Node, "NodeError"]


class NodeError(Exception):
    """TODO"""

    def __init__(self, node: Node, message: str = None):
        super().__init__(message)
        self.node = node


class DeploymentError(Exception):
    """TODO"""

    def __init__(self, errors: NodeErrors, done: NodeSet):
        details = f"{len(errors)} errors, {len(done)} done"
        super().__init__(f"Deployment failed ({details})")
        self.errors = errors
        self.done = done


class RollbackError(Exception):
    """TODO"""

    def __init__(self, dep_errors: NodeErrors, done: NodeSet,
                 errors: NodeErrors, restored: NodeSet):
        details = f"{len(errors)} errors, {len(restored)} restored"
        super().__init__(f"Rollback failed ({details})")
        self.dep_errors = dep_errors
        self.done = done
        self.errors = errors
        self.restored = restored


class FailedDependency(NodeError):
    """TODO"""

    def __init__(self, node: Node, error: NodeError):
        details = f"{node} -> {error.node}, {error}"
        super().__init__(node, f"Failed dependency ({details})")
        self.error = error


class DegradedService(NodeError):
    """TODO"""


class ServiceTimeoutError(NodeError):
    """TODO"""


class StoppedService(NodeError):
    """TODO"""
