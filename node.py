#!/usr/bin/env python3
"""TODO"""
from abc import ABC, abstractmethod
from typing import Dict, ForwardRef, List, Set, Tuple

from .config import Config, Snapshot

# pylint: disable=invalid-name
Service = ForwardRef("Service")
Domain = ForwardRef("Domain")
Host = ForwardRef("Host")
Node = ForwardRef("Node")

NodeNames = Dict[Node, str]
NodeSet = Set[Node]
NodeList = List[Node]
NodeErrors = Dict[Node, Exception]
NodeSnapshots = Dict[Node, Snapshot]

Todo = Tuple[NodeList, NodeList, NodeList]
# pylint: enable=invalid-name


class Node(ABC):
    """Base class for `Host` and `Service` classes, defining some common methods
    used bv the deployment classes (`Deployment` and `DeploymentManager`) and
    by some CLI commands.

    """

    @staticmethod
    def dependencies(nodes):
        """TODO"""
        for node in nodes:
            for dep in node.deps:
                yield node, dep

    @abstractmethod
    async def query(self) -> Config:
        """TODO"""

    @abstractmethod
    async def update(self):
        """TODO"""

    @abstractmethod
    async def snapshot(self, name: str) -> Snapshot:
        """TODO"""

    @abstractmethod
    async def restore(self, snapshot: Snapshot):
        """TODO"""
